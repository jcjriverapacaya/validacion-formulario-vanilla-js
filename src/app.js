import './css/materialize.css';
import "./scss/index.scss";
import './js/materialize';
import './js/index';
import './media/autoprefixer.svg';
import './media/babel.svg';
import './media/close.svg';
import './media/handlebars.svg';
import './media/imagemin.svg';
import './media/postcss.svg';
import './media/sass.svg';
import './media/webpack.svg';
import './media/vscode.svg';

