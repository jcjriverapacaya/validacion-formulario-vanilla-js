
/* ---------- Variables ---------*/
const btnSend = document.getElementById('send'),
      btnReset = document.getElementById('reset'),
      email = document.getElementById('email'),
      asunt = document.getElementById('asunt'),
      message = document.getElementById('message')





/* ---------- Listeners ---------*/
cargarListeners()

function cargarListeners(){
   window.addEventListener('DOMContentLoaded', startApp)
   btnSend.addEventListener('click', sendData)
   btnReset.addEventListener('click',resetData)
   email.addEventListener('blur', validarCampos)
   asunt.addEventListener('blur', validarCampos)
   message.addEventListener('blur', validarCampos)
}


/* ---------- Funciones ---------*/

function startApp(){
   btnSend.disabled = true
}

function sendData(e){
   e.preventDefault()
   const data = {
      email : email.value,
      asunto : asunt.value,
      message : message.value
   }

   console.log(data.email)
   console.log(data.asunto)
   console.log(data.message)
}

function resetData(){

}

function validarCampos(){
   if(email.value !== '' && asunt.value !== '' && message.value !== ''){
      btnSend.disabled = false
   }else {
      btnSend.disabled = true
   }
}
