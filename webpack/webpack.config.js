const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const autoprefixer = require('autoprefixer');
const CopyPlugin = require('copy-webpack-plugin');
const { CleanWebpackPlugin } = require('clean-webpack-plugin')

module.exports = {
   entry: './src/app.js',
   output: {
      path: path.resolve(__dirname, '../public'),
      filename: 'index.[hash].js'
   },
   devServer: {
      port: 4000,
      open: true
   },
   module: {
      rules: [{
            test: /\.hbs$/,
            loader: 'handlebars-loader'
         },
         {
            test: /\.js$/,
            exclude: /node_modules/,
            use: ['babel-loader', 'eslint-loader'],
         },
         {
            test: /\.(sa|sc|c)ss$/,
            use: [
               MiniCssExtractPlugin.loader,
               'css-loader',
               {
                  loader: 'postcss-loader',
                  options: {
                     autoprefixer: {
                        browser: ["last 2 versions"]
                     },
                     plugins: () => [
                        autoprefixer
                     ]
                  }
               },
               'sass-loader',
            ],
         },
         {
            test: /\.(?:ico|gif|png|jpg|jpeg|webp|svg)$/i,
            loader: 'file-loader',
            options: {
               name: '[name].[ext]',
               outputPath: 'assets/media/',
               useRelativePath: true
            }
         },
         {
            loader: 'image-webpack-loader',
            options: {
               bypassOnDebug: true,
               disable: true,
            }
         },
         {
            test: /\.(ttf|eot|woff2?|mp3|mp4|txt|pdf|xml)$/i,
            use: 'file-loader?name=data/[name].[ext]'
         }

      ]
   },

   plugins: [
      new CleanWebpackPlugin(),
      new HtmlWebpackPlugin({
         template: './src/pages/index.hbs',
         minify: {
            html5: true,
            collapseWhitespace: true,
            caseSensitive: true,
            removeComments: true,
            removeEmptyElements: false
         }
      }),
      new MiniCssExtractPlugin({
         filename: "index.[hash].css"
      }),

      new CopyPlugin([{
         from: './src/assets/',
         to: 'assets/',
         ignore: ['*.DS_Store']
      }])


   ]
};
